#include<iostream>
#include<list>
#include <string>
#include<map>
#include<vector>
#include <float.h>
#include <stdlib.h>
#include <math.h>
#include<algorithm>


using namespace std;

struct Point_Pair;

class Point{
public:
  int x_cor;
  int y_cor;
  int index;
  int status; //0= single, 1= taken but looking, 2 = has two edges

  vector<Point*> adjacents;//This should never have more than two
  //The Adjacents list is used to keep track of which elements are next to
  //this specific points
  vector<int> ProposedTo;
  vector <Point*> PreferenceList;
  int marked; //0 for free and 1 for taken, for GS algorithm
  Point * engaged;
  //This one is to keep track of which point each element is
  //"engaged" too for the GS algorithm
  Point(int x, int y, int index);
};

bool operator==(const Point& lhs, const Point& rhs)
{
    return lhs.index == rhs.index;
}

Point::Point( int x, int y, int inde) {
   x_cor = x;
   y_cor =y;
   index = inde;
   status = 0;
   marked =0;
}

void addEdge(Point &P1, Point &P2);
bool acceptableAngle(Point p1, Point p2, Point p3);
float dist(Point &p1, Point &p2);
//This function returns the distance between two points
void compute_nn_edges (Point &p1,  vector<Point> &vec);
void makeEdge(Point &First, Point &Second);
Point* check_if_lowest(Point& P1);
void print_path(vector<Point> &final_vec, int num_points);
bool stillSingle(vector<Point> & vec);
bool comparator(const Point& thispoint,const Point& lhs, const Point& rhs);
bool contains(vector<int>v, int key);
bool containsList(vector<Point> v, Point & key);

int main()
{
  vector <int> X_Coordinates;
  vector <int> Y_Coordinates;

//We use these two vectors to simply store the X and Y Coordinates
//They are later used to make the Point objects

  int num_points;
  cin>>num_points;
  //cout<<"Number of points: "<<num_points<<endl;

  int i =0;
  while (i<num_points)
  {
    i++;
    int x_cor;
    cin>>x_cor;
    cin.ignore();
    int y_cor;
    cin>>y_cor;
    X_Coordinates.push_back(x_cor);
    Y_Coordinates.push_back(y_cor);
  }

//Just to show what we got
   //cout << "The Coordinates are : \n";
   for(int i=0; i < X_Coordinates.size(); i++)
   {
     //cout << X_Coordinates.at(i) << " , ";
     //cout << Y_Coordinates.at(i) << "\n";
   }

   vector <Point> Initial;

   for (int i=0; i<num_points; ++i)
   {
   Point pt(X_Coordinates[i],Y_Coordinates[i],i);
   Initial.push_back(pt);
   }
   //Here the point objects are constructed and pushed back into a vector of points
   //This is where they will stay until we print them out


   for (int i =0; i<Initial.size(); i++)
   {
     compute_nn_edges(Initial[i],Initial);
     //This function simply finds the closest neigbour and adds an "edge" between the two points
     //Keep in mind that in my case an edge is simply adding each other to each elements adjacency lists
   }


//This is simply to show the number of connections each Point OBJ has
   for (int i =0; i<Initial.size();i++)
   {
     //cout<<"The status of "<< Initial[i].index<<" with Coordinates of ("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") is: " <<Initial[i].status<<endl;

   }

   cout<<endl;

   //This loop "Marks" all of the verices with just one edge as Single and Looking
   for (int i =0; i<Initial.size();i++)
   {
     if (Initial[i].status==1)
     {
       //cout<<"("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") is Single and Looking\n";

       Initial[i].marked = 0;

     }
     else
     {
       //cout<<"("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") is NOT LOOKING FOR VERTICES!\n";

       Initial[i].marked = 1;
     }
   }
   cout<<endl;

//This makes the preference lists
   for (int i =0; i<Initial.size();i++)
   {
     for(int j=0; j<Initial.size();j++)
     {
       if(Initial[i].marked ==0 && Initial[j].marked ==0)
       {
      float the_distance=dist(Initial[i],Initial[j]);
       if (the_distance!=0 && Initial[i].adjacents[0]!=&Initial[j])
       {
        // cout<<"The distance is "<<the_distance<<endl;
         Point* temp= &Initial[j];
        //Point_Pair pt_pair(the_distance,&SingleAndLooking[j]);
         Initial[i].PreferenceList.push_back(temp);
       }
     }
     }
   }

   //Prints out the unsorted Preference list
 // for (int i=0; i<Initial.size();i++)
 // {
 //   if (Initial[i].PreferenceList.size()==0)
 //   {
 //     continue;
 //   }
 //   //cout<<"The Preference list of ("<<Initial[i].x_cor<<" , "<<Initial[i].y_cor<<") is: \n";
 //   // for (int j =0; j<Initial[i].PreferenceList.size();j++)
 //   // {
 //   //   cout<<"Num = "<<j+1<<" ("<<Initial[i].PreferenceList[j]->x_cor<<" , "<<Initial[i].PreferenceList[j]->y_cor<< ") distance : "<<dist(Initial[i],*Initial[i].PreferenceList[j])<<endl;
 //   //   //Sort each element
 //   // }
 //  // cout<<endl;
 // }



//   // Do Gales Shapeley
//Initially all p ∈ P are free
//while there is a point that is free and has not proposed to every other point
//  Choose such a point p
//        Let q ∈ P be the highest ranked point in p’s preference list to which P has not yet proposed
//if q is free then
//    (p, q) become engaged
//else q is currently engaged to p`
//    if (Distance of p from q) > (Distance of p`from q) then
//        p remains free
//else (Distance of p from q) < (Distance of p` from q)
//    (p, q) become engaged
//    p` becomes free


int counter=0;

while (stillSingle(Initial) && counter<10 )
{
  // cout<<endl<<counter++<<endl;
  for(int i =0; i<Initial.size();i++)
  {
    if (Initial[i].marked==0)
    {
      Point* temp = check_if_lowest(Initial[i]);
      //cout<<"\nAfter Lowest check func\n";
      Initial[i].ProposedTo.push_back(temp->index);
      if (temp->marked ==0 )
      {
        //cout<<"Engaging ("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") with ("<<temp->x_cor<<","<<temp->y_cor<<")\n";
        Initial[i].engaged=temp;
        temp->engaged =&Initial[i];
        temp->marked=1;
        Initial[i].marked=1;
      }
      else
      {
        if(dist(*temp, *temp->engaged)>dist(*temp,Initial[i]))
        {
        //  cout<<"DIVORCING ("<<temp->x_cor<<","<<temp->y_cor<<") from ("<<temp->engaged->x_cor<<","<<temp->engaged->y_cor<<")\n";
          //cout<<"Engaging ("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") with ("<<temp->x_cor<<","<<temp->y_cor<<")\n";
          Initial[i].engaged=temp;
          temp->engaged= &Initial[i];
          Initial[i].marked=1;
          temp->engaged->marked=0;
          temp->engaged->engaged=NULL;
        }
      }
    }
  }
  // cout<<"\nHere\n";

}

for (int i =0; i < Initial.size();i++)
{
  if(Initial[i].status==1)
  {
    makeEdge(Initial[i],*Initial[i].engaged);
  }

}

print_path(Initial,num_points);












   // vector<Point> final_vec;
   //
   //  for(int i =0; i<num_points;i++)
   //  {
   //    bool pushed=false;
   //    for(int j=0; j<SingleAndLooking.size();j++)
   //    {
   //      if (SingleAndLooking[j].index==i)
   //      {
   //        final_vec.push_back(SingleAndLooking[j]);
   //        pushed=true;
   //      }
   //    }
   //    if (pushed==false)
   //    {
   //      final_vec.push_back(Initial[i]);
   //    }
   //  }





    // for(int i=0;i<Initial.size();i++)
    // {
    //   cout<<"Adjacents of ("<<Initial[i].x_cor<<","<<Initial[i].y_cor<<") are: \n";
    //   for(int j =0; j<Initial[i].adjacents.size();j++)
    //   {
    //     cout<<"("<<Initial[i].adjacents[j]->x_cor<<","<<Initial[i].adjacents[j]->x_cor<<")->";
    //
    //   }
    //   cout<<endl;
    // }





//This is just testing the print function
    // vector<Point> testvec;
    //
    //
    // Point Pt1(-8, 20, 0);
    // Point Pt2(-12, 0, 1);
    // Point Pt3(5, 30, 2);
    // Point Pt4(15, 16, 3);
    // Point Pt5(15, -20, 4);
    // Point Pt6 (0, -20, 5);
    // Pt1.adjacents.push_back(&Pt2);
    // Pt1.adjacents.push_back(&Pt3);
    // Pt2.adjacents.push_back(&Pt1);
    // Pt2.adjacents.push_back(&Pt6);
    // Pt3.adjacents.push_back(&Pt1);
    // Pt3.adjacents.push_back(&Pt4);
    // Pt4.adjacents.push_back(&Pt3);
    // Pt4.adjacents.push_back(&Pt5);
    // Pt5.adjacents.push_back(&Pt4);
    // Pt5.adjacents.push_back(&Pt6);
    // Pt6.adjacents.push_back(&Pt5);
    // Pt6.adjacents.push_back(&Pt2);
    // testvec.push_back(Pt1);
    // testvec.push_back(Pt2);
    // testvec.push_back(Pt3);
    // testvec.push_back(Pt4);
    // testvec.push_back(Pt5);
    // testvec.push_back(Pt6);
    // print_path(testvec,6);























//print_path(Initial, num_points);

  return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////END OF MAIN//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////





void compute_nn_edges (Point &p1,  vector<Point> &vec)
{

  float temp;
  Point * closest;
  if (dist(p1, vec[0])!=0)
  {
    temp =dist(p1,vec[0]);
    closest = &vec[0];
  }
  else
  {
    temp =dist(p1,vec[1]);
    closest = &vec[1];
  }
  for (int i =0; i<vec.size();i++)
  {
    //This takes into account that is NOT itself
    if (dist(p1,vec[i])<temp && dist(p1,vec[i])!=0)
    {
      temp = dist(p1,vec[i]);
      closest = &vec[i];
    }
  }

  //cout<<"The closest element to ("<<p1.x_cor<<","<<p1.y_cor<<") with an index of "<<p1.index<<" is ("<<closest->x_cor<<","<<closest->y_cor<<") with a distance of "<<temp<<" away\n";
  makeEdge(p1,*closest);
  //Makes an Edge between the single closest element

}


void makeEdge(Point &First, Point &Second)
{
    //cout<<"Adjacent of ("<<First.x_cor<<","<<First.y_cor<<") -> ("<<Second.x_cor<<","<<Second.y_cor<<")"<<endl;

//ONLY ADDS THE ELEMENT IF IT IS NOT THERE ALREADY
	if (find(First.adjacents.begin(), First.adjacents.end(), &Second) != First.adjacents.end())
  {
    //cout << "Element found\n";
  }
	else
  {
    First.adjacents.push_back(&Second);
    //cout << "Pushing Back\n";
  }
  if (find(Second.adjacents.begin(), Second.adjacents.end(), &First) != Second.adjacents.end())
  {
  //  cout << "Element found\n";
  }
  else
  {
    Second.adjacents.push_back(&First);
    //cout << "Pushing Back\n";
  }

    First.status=First.adjacents.size();
    Second.status=Second.adjacents.size();
}


//This function returns the distance between two point objects
float dist(Point &p1, Point &p2)
{
    return sqrt( (p1.x_cor - p2.x_cor)*(p1.x_cor - p2.x_cor) +
                (p1.y_cor - p2.y_cor)*(p1.y_cor - p2.y_cor));
}



bool acceptableAngle(Point p1, Point p2, Point p3)
{
  float a = p2.x_cor - p1.x_cor;
  float b = p2.y_cor - p1.y_cor;
  float c = p2.x_cor - p3.x_cor;
  float d = p2.y_cor - p3.y_cor;
  float atanA = atan2(a, b);
  float atanB = atan2(c, d);
  float angle= atanB - atanA;
  float rs = (angle * 180) / 3.141592;
  if (rs<0)
  {
    rs=rs*-1;
  }
  if (rs>=90)
  {
  //  cout<<"The angle is: "<<rs<<" angle is acceptable!"<<endl;
      return true;
  }
  else
  {
  //  cout<<"The angle is: "<<rs<<" angle is NOT ACCEPTABLE!"<<endl;
    return false;
  }
}




Point* check_if_lowest(Point& P1)
{
  if(P1.PreferenceList.size()!=0)
  {

    Point* temp;
    if (!contains(P1.ProposedTo, P1.PreferenceList[0]->index))
    {
      temp=P1.PreferenceList[0];
    }
    else
    {
      int j = 0;
    while( j<P1.PreferenceList.size() )
    {
      //cout<<"In while loop";
      if (!contains(P1.ProposedTo, P1.PreferenceList[j]->index))
      {
        temp=P1.PreferenceList[j];
        break;

      }

      j++;
    }
  }

    for (int i =0; i< P1.PreferenceList.size();i++)
    {
    //  cout<<"Distances of ("<<P1.x_cor<<", "<<P1.y_cor<<")"<<P1.PreferenceList[i].distance<<endl;
      if ((dist(P1,*P1.PreferenceList[i])< dist(P1,*temp))&& !contains(P1.ProposedTo, P1.PreferenceList[i]->index))
      {
        temp = P1.PreferenceList[i];
      }
    }
   //cout<<"The lowest distance of ("<<P1.x_cor<<", "<<P1.y_cor<<") is  "<<temp<<endl;

    return temp;

  }

  else
  {
    return NULL;
  }

}

bool stillSingle(vector<Point> & vec)
{
  for (int i =0; i< vec.size();i++)
  {
    if (vec[i].marked==0)
    {
      //cout<<"("<<vec[i].x_cor<<","<<vec[i].y_cor<<") is STILL SINGLE"<<endl;
      return true;
    }

  }
//cout<<"NO MORE SINGLES!!!\n";
return false;

}




void print_path(vector<Point> &final_vec, int num_points)
{
//cout<<"PATH =\n";

vector<int> visited;
//cout<<final_vec[0].index<<endl;
visited.push_back(final_vec[0].index);
Point *traversal_ptr = final_vec[0].adjacents[0];
int i =0;
//cout<<"\nGetting to loop\n";
//cout<<"("<<final_vec[0].x_cor<<","<<final_vec[0].y_cor<<")->";
cout<<final_vec[0].index<<endl;//<<"->("<<final_vec[0].x_cor<<","<<final_vec[0].y_cor<<")\n";
//cout<<"Before Loop";
while (i<num_points-1)
  {
    visited.push_back(traversal_ptr->index);
    cout<<traversal_ptr->index<<endl;//<<"->("<<traversal_ptr->x_cor<<","<<traversal_ptr->y_cor<<")\n";
    //cout<<"("<<traversal_ptr->x_cor<<","<<traversal_ptr->y_cor<<")->";
    //cout<<traversal_ptr->index<<endl;

    if(contains(visited,traversal_ptr->adjacents[0]->index) && contains(visited,traversal_ptr->adjacents[1]->index))
    {
      //cout<<"\nContains Both!\n";
      break;
    }

    if(contains(visited,traversal_ptr->adjacents[0]->index))
    {
      traversal_ptr=traversal_ptr->adjacents[1];
    }

    else
    {
      traversal_ptr=traversal_ptr->adjacents[0];
    }
     i++;
  }
  cout<<final_vec[0].index<<endl;//<<"->("<<final_vec[0].x_cor<<","<<final_vec[0].y_cor<<")\n";
  //cout<<"("<<final_vec[0].x_cor<<","<<final_vec[0].y_cor<<")\n\n\n\n";
}


bool contains(vector<int> v, int key)
{
  if(std::find(v.begin(), v.end(), key) != v.end()) {
    return true;
    /* v contains x */
}
else
{
  return false;
    /* v does not contain x */
}


}



//if (find(First.adjacents.begin(), First.adjacents.end(), &Second) != First.adjacents.end())
