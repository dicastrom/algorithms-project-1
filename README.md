<h1>READ ME</h1><br>
<h2>Project 1 Algorithms FSU FALL 2020</h2><br>
<h2>Diego Castro, djc16j</h2><br>
This project was a curve reconstruction algorithm.
I made mine by making point objects, making the initial edges using
the compute n edges function,then I created preference lists for each "Single and Looking:
Point object. Then I used the modified Gale Shapely Algorithm for finding the second
edge for all the "Single and Looking" points. Finally, I used my print function,
which uses a vector to store the points that were already visited and keep on
going through the adjacents till all the point objects' indexes have been
printed out.
The time complexity my algorithm is not too great as the Gale Shapely bit has a
complexity of O(N^3). However, it does run super quick on my computer at least,
even with relatively large inputs.


As for the output, the specification were to print out the Indexes, which does not look that great in my opinion, so I do have the option
of printing out the x,y coordinates. You simply need to un-comment a couple of lines in the print path function and it should look better.




<h3>To Run:</h3>
 download the main.cpp file
 download makefile
 write make in terminal
 write make
 write number of points
 write down the points (optional separate with a comma)
 DONE!
 You should now see the output of points
 or 
 <h3>
 main < input.txt
 </h3>



<h3>
 Alternatively:
 </h3>
 download the main.cpp file
 compile using :

<h2>
 g++ main.cpp -o main
 </h2>
 This will make a main executable
 write make
 write number of points
 write down the points (optional separate with a comma)
 DONE!
 You should now see the output of points
  or 
 main < input.txt


<h3>Sample Input:<br></h3>
8<br>
2,2<br>
4,1<br>
4,4<br>
2,3<br>
5,3<br>
3,4<br>
3,1<br>
5,2<br>

<h3>Sample Output:<br></h3>

0<br>
3<br>
5<br>
2<br>
4<br>
7<br>
1<br>
6<br>
0<br>


<h3>Sample Input 2:<br></h3>
17<br>
0, 0<br>
0, 1<br>
1, 2<br>
2, 3<br>
4, 3<br>
5, 3<br>
6, 2<br>
7, 1<br>
8, 0<br>
7, -1<br>
6, -2<br>
5, -3<br>
4, -3<br>
3, -3<br>
2, -3<br>
1, -2<br>
0, -1<br>

<h3>
Sample Output 2:<br>
<h3>

0<br>
1<br>
2<br>
3<br>
4<br>
5<br>
6<br>
7<br>
8<br>
9<br>
10<br>
11<br>
12<br>
13<br>
14<br>
15<br>
16<br>
0<br>
<h3>
<br>
<h3>
Sample Input 3:<br>
</h3>
8<br>
-8 0<br>
-6 4<br>
0 6<br>
4 2<br>
10 -2<br>
6 -4<br>
0 -4<br>
-4 -2<br>

Notice no commas
<h3>
Sample Output 3:<br>
</h3>

0<br>
1<br>
2<br>
3<br>
5<br>
4<br>
6<br>
7<br>
0<br>



